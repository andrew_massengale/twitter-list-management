var _ = require('underscore');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');


var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/api/twitter/users/:sproutUserID', function (req, res) {
	var sproutUserID = req.params.sproutUserID;

	var returnJSON = [ ];

	switch (sproutUserID) {
	case '1':
		returnJSON = [
			{ userID: 4, userName: 'testuser4', pictureURL: '/testpicture4' },
			{ userID: 5, userName: 'testuser5', pictureURL: '/testpicture5' },
			{ userID: 6, userName: 'testuser6', pictureURL: '/testpicture6' }
		];
		break;
	case '2':
		returnJSON = [
			{ userID: 7, userName: 'testuser7', pictureURL: '/testpicture7' },
			{ userID: 8, userName: 'testuser8', pictureURL: '/testpicture8' },
			{ userID: 9, userName: 'testuser9', pictureURL: '/testpicture9' }
		];
		break;
	default:
		returnJSON = [
			{ userID: 1, userName: 'andrewm', pictureURL: '/testpicture' },
			{ userID: 2, userName: 'testuser2', pictureURL: '/testpicture2' },
			{ userID: 3, userName: 'testuser3', pictureURL: '/testpicture3' }
		];
		break;
	}
	res.jsonp(returnJSON);
});

app.get('/api/twitter/lists/for/:twitterUserID', function (req, res) {
	var twitterUserID = req.params.twitterUserID;

	var returnJSON = [ ];

	var twitterLists = [
		{ name: 'List 1', id: 1 },
		{ name: 'List 2', id: 2 },
		{ name: 'List 3', id: 3 },
		{ name: 'List 4', id: 4 },
		{ name: 'List 5', id: 5 },
		{ name: 'List 6', id: 6 },
		{ name: 'List 7', id: 7 },
		{ name: 'List 8', id: 8 },
		{ name: 'List 9', id: 9 },
		{ name: 'List 10', id: 10 },
	];

	_(twitterLists).each(function (list) {
		if (randomBool()) returnJSON.push(list);
	});

	res.jsonp(returnJSON);
});

app.get('/api/is/user/:twitterUserID/in/list/:listID', function (req, res) {
	var twitterUserID = req.params.twitterUserID;
	var listID = req.params.listID;

	var returnJSON = { twitterUserInList: randomBool() };

	res.jsonp(returnJSON);
});

app.all('/api/twitter/user/:userID/in/list/:listID', function (req, res) {
	res.jsonp({ success: true });
});

function randomBool() {
	return (Math.round(Math.random()));
}

module.exports = app;