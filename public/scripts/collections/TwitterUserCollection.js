/** Local collection of player responses to provide easy calculation of scores and quick lookup of previous answers
  *
  * @class TwitterUserCollection
  * @constructor
  * @param {object} TwitterUserModel Player response data model class
  */
define(
	[ 'models/TwitterUser', 'backbone' ],

	function (TwitterUserModel, Backbone) {
		var TwitterUserCollection = Backbone.Collection.extend({
			model: TwitterUserModel,

			// the sprout user id we'll use to build our list of models
			sproutUserID: '',
			// how many Twitter user lists we've built so far
			completedTwitterUserLists: 0,

			/** Initialize the collection and add a few event listeners
			  *
			  * @method initialize
			  * @param {int} sproutID The id of the sprout user we'll use to get all of their
			  * associated twitter profiles
			  */
			initialize: function (sproutID) {
				var collection = this;
				collection.sproutUserID = sproutID;

				collection.on('lists:add', collection.twitterListFinished);
				collection.on('lists:update', collection.updateTwitterList);
			},

			/** Called once a Twitter list is finished building on a model. What this function does
			  * is keep a running total of Twitter lists that have finished, and once they're all
			  * done, it fires off a Backbone event to let the view know that it can render.
			  *
			  * @method twitterListFinished
			  * @param {Object} The model that finished updating it's Twitter lists
			  */
			twitterListFinished: function (twitterUser) {
				var collection = this;

				collection.completedTwitterUserLists++;
				if (collection.completedTwitterUserLists === collection.length) {
					collection.trigger('lists:complete', collection);
				}
			},

			/** Called when a model needs to update the status of one of it's Twitter lists. Called
			  * for either adding a list from a Twitter user or removing one.
			  *
			  * @method updateTwitterList
			  * @param {int} listItemID The id of the list item to be modified on the Twitter user
			  * @param {int} twitterUserID The id of the Twitter user to be modified
			  * @param {Boolean} currentlyInList The current state of the list's association with
			  * the Twitter user. The list/user relationship will be toggled based off this state.
			  */
			updateTwitterList: function (listItemID, twitterUserID, currentlyInList) {
				var collection = this;

				// find the model to update
				var modelToUpdate = collection.findWhere({ userID: twitterUserID });
				// if the user is currently in the list, remove them; if they're not in the list, add them
				if (currentlyInList) {
					modelToUpdate.removeListFromTwitterUser(listItemID);
				} else {
					modelToUpdate.addListToTwitterUser(listItemID);
				}
			},

			// dynamically build the API route to create our models based off the sproutUserID that's passed
			// in on initialization
			url: function () {
				return '/api/twitter/users/' + this.sproutUserID;
			}
		});

		return TwitterUserCollection;
	}
)