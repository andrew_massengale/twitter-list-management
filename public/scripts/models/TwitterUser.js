/** Model for a specific twitter user.
  *
  * @class TwitterUserModel
  * @constructor
  */
define(
	[ 'async', 'backbone' ],

	function (async, Backbone) {
		var TwitterUserModel = Backbone.Model.extend({
			defaults: {
				userID: '',
				userName: '',
				pictureURL: '',
				lists: []
			},

			// there can only be 1000 lists on a twitter profile
			maxListsPerTwitterUser: 1000,

			idAttribute: 'userID',

			// various API skeleton routes we'll be needing
			getListsURL: '/api/twitter/lists/for/:userID',
			getListStatusURL: '/api/is/user/:userID/in/list/:listID',
			updateListForTwitterUserURL: '/api/twitter/user/:userID/in/list/:listID',

			/** Nothing to do when initing this model
			  *
			  * @method init
			  */
			init: function () {
				// nothing to do
			},

			/** Adds a list to a Twitter user through an API call, and updates the current model
			  *
			  * @method addListToTwitterUser
			  * @param {int} listID The id of the list to add to the model
			  */
			addListToTwitterUser: function (listID) {
				var model = this;
				var returnObj = { success: false };

				// make sure we create a new array with a new array reference, so Backbone knows it's been updated
				var lists = JSON.parse(JSON.stringify(model.get('lists')));

				if (lists.length >= model.maxListsPerTwitterUser) {
					return false;
				}

				var matchingList = _(lists).findWhere({ id: listID });
				if (!matchingList) {
					return false;
				}
				var listIndex = lists.indexOf(matchingList);

				var url = model.updateListForTwitterUserURL
					.replace(':userID', model.get('userID'))
					.replace(':listID', listID);
				$.ajax({ url: url, type: 'PUT', success: function (retObj) {
					if (!retObj.success) return false;

					lists[listIndex].twitterUserInList = 1;
					model.set('lists', lists);
				}});
			},

			/** Removes a list to a Twitter user through an API call, and updates the current model
			  *
			  * @method addListToTwitterUser
			  * @param {int} listID The id of the list to remove from the model
			  */
			removeListFromTwitterUser: function (listID) {
				var model = this;
				// I'm doing this weird JSON stringifying/parsing to make sure we create a new array
				// with a new array reference, so Backbone knows it's been updated
				var lists = JSON.parse(JSON.stringify(model.get('lists')));

				if (lists.length >= model.maxListsPerTwitterUser) {
					return false;
				}

				var matchingList = _(lists).findWhere({ id: listID });
				if (!matchingList) {
					return false;
				}
				var listIndex = lists.indexOf(matchingList);

				var url = model.updateListForTwitterUserURL
					.replace(':userID', model.get('userID'))
					.replace(':listID', listID);
				$.ajax({ url: url, type: 'DELETE', success: function (retObj) {
					if (!retObj.success) return false;

					lists[listIndex].twitterUserInList = 0;
					model.set('lists', lists);
				}});
			},

			/** This overrides the built in Backbone sync functionality. The reason for this is
			  * becuause we need to make several API calls to get all the model information we
			  * need.
			  *
			  * @method sync
			  */
			sync: function () {
				var twitterUser = this;

				async.waterfall([
					// get all the twitter lists for this user
					function getTwitterLists(cb) {
						var url = twitterUser.getListsURL.replace(':userID', twitterUser.get('userID'));

						$.ajax({ url: url, success: function (lists) {
							twitterUser.set('lists', lists);

							cb();
						}});
					},

					// once we have the lists, we need to loop through each one, and see if this twitter
					// user has connected with that list, and mark the list as such in the model
					function getListStatus(cb) {
						var twitterLists = twitterUser.get('lists');
						var newTwitterLists = [];

						// i'm using an async.queue here to get the twitter lists statuses in parallel of
						// each other (or at least 10 at a time, so we don't overload the server)
						function getListStatusFromServer(list, qCB) {
							var listStatusURL = twitterUser.getListStatusURL
								.replace(':userID', twitterUser.get('userID'))
								.replace(':listID', list.id);

							$.ajax({ url: listStatusURL, success: function (listStatus) {
								list.twitterUserInList = (listStatus.twitterUserInList);
								newTwitterLists.push(list);

								qCB();
							}});
						}

						var twitterListStatusQ = new async.queue(getListStatusFromServer, 10);
						// once we're done, set the updated twitter lists we've been building back on the model
						twitterListStatusQ.drain = function () {
							twitterUser.set('lists', newTwitterLists);
							cb();
						};
						twitterListStatusQ.push(twitterLists);
					}
				], function () {
					// trigger an event, so we know when we can begin displaying the data to the view
					twitterUser.trigger('lists:add', twitterUser);
				});
			}
		});

		return TwitterUserModel;
	}
);