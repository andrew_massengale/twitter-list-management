'use strict';

// specify the core libraries, and library dependencies needed
require.config({
	baseUrl: '/scripts',
	paths: {
		'backbone': 'lib/backbone',
		'jquery': 'lib/jquery',
		'underscore': 'lib/underscore',
		'async': 'lib/async',
		'text': 'lib/text'
	},
	shim: {
		'backbone': {
			deps: [ 'jquery', 'underscore' ]
		},
	}
});

require(
	[ 'backbone', 'controllers/app' ],
	function (Backbone, app) {
		// All we're doing here is initializing the app
		app.init();
	}
);