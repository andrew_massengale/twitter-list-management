/** Popover content manager for the Twitter user list management
  *
  * @class TwitterUserView
  * @constructor
  * @param {string} twitterUserTemplate Underscore template string
  */
define(
	[ 'text!templates/TwitterUser.html', 'backbone' ],

	function (twitterUserTemplate, Backbone) {
		var TwitterUserView = Backbone.View.extend({
			template: _.template(twitterUserTemplate),

			/** On initialization, create a listener for when all of the Twitter
			  * lists are finished, so we can begin rendering
			  *
			  * @method initialize
			  */
			initialize: function () {
				var view = this;

				view.collection.on('lists:complete', view.initView, view);
			},

			/** This function begins rendering the view. It also adds a few listeners
			  * as well
			  *
			  * @method initView
			  * @return {Object} TwitterUserView instance (this)
			  */
			initView: function () {
				var view = this;

				view
					.render()
					._initButtonListener()
					._initListItemListener()
					.listenTo(view.collection, 'change', function (changedModel) {
						view.partialListRender(changedModel);
					});

				return view;
			},

			/** Create a listener for each Twitter list item in order to toggle the Twitter
			  * user being in that list. Fires a Backbone trigger everytime one is clicked.
			  *
			  * @method _initListItemListener
			  * @private
			  * @return {Object} TwitterUserView instance (this)
			  */
			_initListItemListener: function () {
				var view = this;

				view.$el.delegate('.twitter-list-item', 'touchstart mousedown', function () {
					var listItemID = $(this).data('id');
					var twitterUserID = $(this).parents('.twitter-user').data('id');
					var currentlyInList = $(this).children('.checkbox').hasClass('checked');

					view.collection.trigger('lists:update', listItemID, twitterUserID, currentlyInList);
				});

				return view;
			},

			/** Creates a listener for the button to show the popover. Also, enables the button.
			  *
			  * @method _initButtonListener
			  * @private
			  * @return {Object} TwitterUserView instance (this)
			  */
			_initButtonListener: function () {
				var view = this;

				$targetDiv = $('#twitterUserListManagement');
				$('#showTwitterListsPopover')
					.removeAttr('disabled')
					.on('touchstart mousedown', function () {
						if ($targetDiv.is(':hidden')) {
							$targetDiv.css('display', 'inline-block');
						} else {
							$targetDiv.css('display', 'none');
						}
					}
				);

				return view;
			},

			/** Renders all of Twitter users to the popover
			  *
			  * @method render
			  * @return {Object} TwitterUserView instance (this)
			  */
			render: function () {
				var view = this;

				var html = _(view.collection.models).reduce(function (currentHTML, model) {
					return currentHTML + view.template(model.toJSON());
				}, '');
				view.$el.html(html);

				return view;
			},

			/** Only render a specific Twitter user's portion of the popover
			  *
			  * @method partialListRender
			  * @param {Object} model The Twitter user's model to be rendered
			  * @return {Object} TwitterUserView instance (this)
			  */
			partialListRender: function (model) {
				var view = this;
				var modelJSON = model.toJSON();

				var $twitterUser = $('.twitter-user[data-id="' + modelJSON.userID + '"]');
				var html = view.template(model.toJSON());
				$twitterUser.html(html);

				return view;
			}
		});

		return TwitterUserView;
	}
)