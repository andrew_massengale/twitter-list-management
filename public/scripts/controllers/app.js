/** Main app controller - creates the base instances of models & views.
  *
  * @class AppController
  * @param {object} TwitterUserModel A model for a Twitter user
  * @param {object} TwitterUserCollection A collection of Twitter user models
  * @param {object} TwitterUserView The view used to render multiple Twitter users to the screen.
  * @return {object} Object-literal containing public functions (initGame)
  */
define(
	[
		'models/TwitterUser',
		'collections/TwitterUserCollection',
		'views/TwitterUserView'
	],

	function (TwitterUserModel, TwitterUserCollection, TwitterUserView) {
		// Set up local aliases for model instances that can be shared in the scope of this module.
		var twitterUserModel, twitterUserCollection, twitterUserView;

		// in a real life scenario, this would probably be retrived via localStorage or another API call
		var sproutSocialID = 3;

		// create a base app for initialization
		var app = {
			init: init
		};
		return app;

		/** Create the base models & views, wire-up any Backbone event listeners, and begin fetching model data
		  *
		  * @method init
		  */
		function init() {
			// Create the base models and views, then begin loading model data
			twitterUserModel = new TwitterUserModel();
			twitterUserCollection = new TwitterUserCollection(sproutSocialID);
			twitterUserView = new TwitterUserView({ collection: twitterUserCollection, el: $('#twitterUserListManagement')[0] });

			// here is where we fetch the model data
			// once it's done, we need to load the twitter lists, since those are separate API calls
			twitterUserCollection.fetch({ success: _initTwitterLists });
		}

		/** Invoke the twitter list syncing function on each model in the collection
		  *
		  * @method _initTwitterLists
		  * @private
		  */
		function _initTwitterLists() {
			twitterUserCollection.invoke('sync');
		}
	}
)