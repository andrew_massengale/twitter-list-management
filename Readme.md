# How to build the server
To start this node server make sure you have nodejs, and npm installed, then run:

1. `npm install` to configure the server, and

2. `npm start` to run the server.

You can view the server at http://localhost:3000 once it's running.

All of my relevant Backbone JavaScript code is in the folder /public/scripts.